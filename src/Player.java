
public class Player {

	public int playerLocation;
	public int money;
	public int awarenessLevel;
	public int moneyForNextLevel;
	public boolean gameRunning;

	public Player(int moneyTotal, int playerStartPos) {
		this.money = moneyTotal;
		this.playerLocation = playerStartPos;
		PrintPlayerInfo();
	}
	
	public void PrintPlayerInfo() {
		System.out.println("Your Money: " + this.money + " | Location Level: " + this.playerLocation);
		
	}
	


}
