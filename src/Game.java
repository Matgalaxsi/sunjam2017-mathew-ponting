import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Mathew
 *
 */
public class Game {
	
	public Player playerReference;
	public int floodLevel;
	public int homelessPeople;
	public int turnCount;
	public boolean gameRunning;
	public int moveCount;
	
	public int cost_awareness;
	
	public Game() { 
		System.out.println("Game has been initialized");
		playerReference = new Player(5000, 1);
		cost_awareness = 300;
		playerReference.moneyForNextLevel = 500;
		RunGame();
	}
	
	public void RunGame() {
		gameRunning = true;
		while(gameRunning == true) {
			System.out.println("\nTurn " + turnCount + ".\nFlood Level: " + floodLevel + "\nCommands Avaliable: Go Inland (G) ($" + playerReference.moneyForNextLevel +") - Raise Awareness (A) ($" + cost_awareness +") - My Info (I) - Help (H) - End Turn (E) - World Info (W)");
			System.out.println("What do you wish to do?\n");
			Scanner input = new Scanner(System.in);
	        String moveCommand = input.nextLine();
	        System.out.println("You have entered: " +moveCommand);
	        Command(moveCommand);
	        if(moveCount == 3) {
	        	EndTurn();
	        }
		}
		
	}
	
	public void Command(String commandText) {
		if(commandText.equals("Raise Awareness") || commandText.equals("A")) {
			if(playerReference.money < cost_awareness) {
				System.out.println("You don't have enough money for this.");
			}
			else {
				RaiseAwareness("");
			}
		}
		
		else if(commandText.equals("My Info") || commandText.equals("I")) {
			playerReference.PrintPlayerInfo();
		}
		
		else if(commandText.equals("End Turn") || commandText.equals("E")) {
			playerReference.PrintPlayerInfo();
		}
		
		else if(commandText.equals("Go Inland") || commandText.equals("G")) {
			GoInland("");
		}
		
		else if(commandText.equals("World Info") || commandText.equals("W")) {
			ShowWorldInfo();
		}
		
		else if(commandText.equals("Help") || commandText.equals("H")) {
			System.out.println("Commands Avaliable: Go Inland (G) - Raise Awareness (A) - My Info (I) - Help (H) - End Turn (E) - World Info (W)");
		}
	}
	
	public void ShowWorldInfo() {
		System.out.println("\nFlood Level: " + floodLevel + "\nHomeless People: " + homelessPeople + "\nMoves Made this turn: " + moveCount);
	}
	
	public void GoInland(String T) {
		if(playerReference.money < playerReference.moneyForNextLevel) {
			System.out.println("You don't have enough money to move further inland.");
		}
		
		else {
			System.out.println("Moving Inland will cost money ($"+playerReference.moneyForNextLevel+")! Are you sure you want to do this? (Y/N)");
			if(T.equals("")) {
				Scanner input = new Scanner(System.in);
		        T = input.nextLine();
			}
			if(T.equals("Y")) {
				playerReference.money -= playerReference.moneyForNextLevel;
				playerReference.moneyForNextLevel += 500;
				playerReference.playerLocation ++;
				System.out.println("You move further inland!.");
				moveCount++;
			}
			
			else if (T.equals("N")) {
				System.out.println("You decide not to move further inland!.");
			}
			
			else {
				System.out.println("Invalid Answer.");
				RaiseAwareness("");
			}
		}
	}
	
	public void RaiseAwareness(String T) {
		System.out.println("Raise Awareness of the Situation will cost money ($"+cost_awareness+")! Are you sure you want to do this? (Y/N)");
		if(T.equals("")) {
			Scanner input = new Scanner(System.in);
	        T = input.nextLine();
		} 
		
		if(T.equals("Y")) {
			playerReference.money -= cost_awareness;
			playerReference.awarenessLevel ++;
			cost_awareness += 100;
			System.out.println("You raise awareness of the rising sea levels.");
			moveCount++;
		}
		
		else if (T.equals("N")) {
			System.out.println("You decide not to raise awareness of the rising sea level.");
		}
		
		else {
			System.out.println("Invalid Answer.");
			RaiseAwareness("");
		}
	}
	
	public void EndTurn() {
		moveCount = 0;
		System.out.println("Turn " + turnCount + " has ended. Calculating results...");
		turnCount++;
		floodLevel++;
		if(floodLevel == playerReference.playerLocation) {
			System.out.println("You have lost your homes in the floods, you are now homeless. Game Over.");
			gameRunning = false;
			return;
		}
		
		if(playerReference.awarenessLevel > 0) {
			playerReference.money += 100 * playerReference.awarenessLevel;
				
			}

		if(playerReference.awarenessLevel < turnCount) {
			playerReference.awarenessLevel -= 1;
			if(playerReference.awarenessLevel < 0) {
				playerReference.awarenessLevel = 0;
			}
			
			homelessPeople += 1000;
			
		}
		
		if(homelessPeople == 10000) {
			System.out.println("You have failed in making people aware of the rising water levels!. Game Over.");
			gameRunning = false;
			return;
		}

		playerReference.money += 100 * turnCount;
	}
}
