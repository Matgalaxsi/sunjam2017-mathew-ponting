import java.util.ArrayList;
import java.util.Random;

public class Lists {
	ArrayList<String> awarenessIssues = new ArrayList<String>();
	
	Lists() {
		// Raising Awareness List
		awarenessIssues.add("You run down the street in your Birthday Suit screaming GLOBAL WARMING IS REAL! Some people notice you and have realised that it is issue after of course!");
		awarenessIssues.add("You run a protest down near the mayor's office and people notice and join you in your protest.");
		awarenessIssues.add("You go down to your local fish and chip shop and start talking about global warming. People start to notice and have taken what you've said into account.");
		awarenessIssues.add("You go down to the night club and chat up a few people. Some of them become your friends and they start making people aware of the situation.");
		awarenessIssues.add("You go to the Local University and crash a couple of classes just to talk about the rising issue. Even though you get kicked out of the University, you make some more people aware of what is happening.");
		awarenessIssues.add("Your work has a party and you decide to tell everyone about the rising issue. They don't beleive you at first but when you show them comparison, it scares the living out of them and it makes them aware.");	
	}
	
	public String GetRandomStringValue(ArrayList listReference) { 
		String value = "";
		Random randomizer = new Random();
		value = (String) listReference.get(randomizer.nextInt(listReference.size()));
		return value;
	}
}
